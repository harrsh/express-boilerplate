import mongoose from 'mongoose';
import logger from './logger';
import env from '../env';

const connectDB = async () => {
    await mongoose.connect(env.mongo.mongoURI);
    mongoose.set('debug', true);

    logger.info('MongoDB connected...');
};

export default connectDB;
