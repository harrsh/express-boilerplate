import * as dotenv from 'dotenv';
import { getOsEnv } from './libs/env';

dotenv.config();

/**
 * Environment variables
 */
const env = {
    node: getOsEnv('APP_ENV'),

    app: {
        name: getOsEnv('APP_NAME'),
        host: getOsEnv('APP_URL'),
        port: getOsEnv('APP_PORT'),
        root_dir: getOsEnv('APP_ENV') === 'production' ? 'dist' : 'src',
    },

    api: {
        api_prefix: getOsEnv('API_PREFIX'),
        user_uploaded_content_path: getOsEnv('USER_UPLOADED_CONTENT_PATH'),
    },

    cors: {
        urls: getOsEnv('CORS_AVAILABLE_LINKS').split(','),
    },

    mongo: {
        mongoURI: getOsEnv('MONGO_URI'),
    },

    jwt: {
        secret: getOsEnv('JWT_SECRET'),
        expiration: getOsEnv('JWT_EXPIRATION'),
    },
};

export default env;
