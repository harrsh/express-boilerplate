import { NextFunction, Request, Response } from 'express';
import logger from '../providers/logger';

const RequestLogger = (req: Request, res: Response, next: NextFunction) => {
    logger.info(`Route: ${req.method} ${req.originalUrl}`);

    next();
};

export default RequestLogger;
