import { Router } from 'express';
import Ping from '../controllers';
import AdminRouter from './admin';

const router = Router();

router.get('/', Ping);

// Admin routes
router.use('/admin', AdminRouter);

export default router;
