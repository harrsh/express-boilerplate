import { Router } from 'express';
import AuthRouter from './auth.router';

const AdminRouter = Router();

// Admin authentication
AdminRouter.use('/auth', AuthRouter);

export default AdminRouter;
