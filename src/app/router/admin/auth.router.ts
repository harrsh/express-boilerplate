import { Router } from 'express';
import Admin_SignIn_Controller from '../../controllers/admin/auth/signIn.controller';
import requestValidator from '../../middlewares/requestValidator';
import {
    Admin_SignIn_Request,
    Admin_VerifySignInOtp_Request,
} from '../../requests/admin/auth.request';
import Admin_VerifySignInOtp_Controller from '../../controllers/admin/auth/verifySignInOtp.controller';

const AuthRouter = Router();

AuthRouter.post(
    '/signin',
    requestValidator(Admin_SignIn_Request),
    Admin_SignIn_Controller
);

AuthRouter.post(
    '/verify-signin-otp',
    requestValidator(Admin_VerifySignInOtp_Request),
    Admin_VerifySignInOtp_Controller
);

export default AuthRouter;
