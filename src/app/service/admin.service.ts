import Admin, { IAdminSchema } from '../schemas/admin.schema';
import { FilterQuery, UpdateQuery } from 'mongoose';

export const AdminFindOne = async (args: FilterQuery<IAdminSchema>) => {
    const admin = await Admin.findOne({
        ...args,
        deletedAt: null,
    });

    return admin;
};

export const AdminUpdateOneById = async (
    id: string,
    args: UpdateQuery<IAdminSchema>
) => {
    const updatedAdmin = await Admin.findByIdAndUpdate(id, args, {
        new: true,
    });

    return updatedAdmin;
};

export const AdminDeleteMany = async (args?: FilterQuery<IAdminSchema>) => {
    await Admin.deleteMany(args);

    return true;
};
