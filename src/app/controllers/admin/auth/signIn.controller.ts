import { Request, Response } from 'express';
import response from '../../../libs/response';
import {
    AdminFindOne,
    AdminUpdateOneById,
} from '../../../service/admin.service';
import { comparePassword } from '../../../utils/managePassword';
import { generateOTP } from '../../../utils/otp';
import { Admin_SignIn_RequestType } from '../../../requests/admin/auth.request';

/**
 * Sign in for admin.
 * @url     /admin/auth/signin
 * @access  Public
 * @method  POST
 */
const Admin_SignIn_Controller = async (req: Request, res: Response) => {
    try {
        const {
            body: { email, contactNumber, password },
        }: Admin_SignIn_RequestType = req.body.parsedData;

        const admin = await AdminFindOne({
            $or: [
                {
                    email: {
                        email,
                    },
                },
                {
                    contactNumber: {
                        contactNumber,
                    },
                },
            ],
        });
        if (!admin) {
            throw new Error('Admin not found.');
        }

        const doesPasswordMatch = await comparePassword(
            password,
            admin.password.password
        );
        if (!doesPasswordMatch) {
            throw new Error('Your password does not match.');
        }

        const otp = generateOTP();
        await AdminUpdateOneById(admin.id, {
            loginOtp: {
                otp,
                otpSentAt: new Date(),
            },
        });

        return response(req, res, {
            message: 'OTP has been sent to your mobile number.',
        });
    } catch (error) {
        return response(req, res, null, error);
    }
};

export default Admin_SignIn_Controller;
