import { NextFunction, Request, Response } from 'express';
import _ from 'lodash';
import { AnyZodObject, ZodEffects } from 'zod';
import response from '../libs/response';

const requestValidator =
    (schema: ZodEffects<AnyZodObject> | AnyZodObject) =>
    async (req: Request, res: Response, next: NextFunction) => {
        try {
            const parsedData = await schema.parseAsync({
                body: req.body,
                query: req.query,
                params: req.params,
            });

            req.body.parsedData = parsedData;

            return next();
        } catch (error) {
            console.log(
                '------------------------------------------------------------'
            );
            console.log('Error while validating the request...');
            console.log(JSON.stringify(error, null, 2));
            console.log(
                '------------------------------------------------------------'
            );

            const responseData = {
                success: false,
                message: _.get(error, 'issues[0].message'),
            };
            return response(req, res, responseData);
        }
    };

export default requestValidator;
