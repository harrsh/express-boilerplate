import { object, string, z } from 'zod';

// Sign in for admin.
export const Admin_SignIn_Request = object({
    body: object({
        email: string().email('Not a valid email.'),
        contactNumber: string(),
        password: string({
            required_error: 'Password is required.',
        }),
    })
        .partial({
            email: true,
            contactNumber: true,
        })
        .refine(
            (data) => data.email || data.contactNumber,
            'Either email or contact number should be filled in.'
        ),
});
export type Admin_SignIn_RequestType = z.infer<typeof Admin_SignIn_Request>;

// Verify OTP for admin sign in.
export const Admin_VerifySignInOtp_Request = object({
    body: object({
        email: string().email('Not a valid email.'),
        contactNumber: string(),
        otp: string({
            required_error: 'OTP is required.',
        }),
    })
        .partial({
            email: true,
            contactNumber: true,
        })
        .refine(
            (data) => data.email || data.contactNumber,
            'Either email or contact number should be filled in.'
        ),
});
export type Admin_VerifySignInOtp_RequestType = z.infer<
    typeof Admin_VerifySignInOtp_Request
>;
